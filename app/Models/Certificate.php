<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
    use HasFactory;

    protected $fillable = [
        'type',
        'name',
        'from_date',
        'to_date',
        'content',
        'file_name',
        'notes',
        'remind_days',
    ];
}
