<?php
namespace App\Services;

use App\Enums\CertificateTypeEnum;
use App\Models\Certificate;
use Exception;
use Illuminate\Foundation\Application;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class CertificateService
{
    protected $app;

    protected $allowedFormats = [
        '.p12' => 'application/x-pkcs12'
    ];

    /**
     * CertificateService constructor.
     * @param Application $app
     */
    public function __construct (Application $app) {
        $this->app = $app;
    }

    /**
     * @param string $url
     * @param array $certificateProps
     * @return Certificate
     * @throws Exception
     */
    public function getWebCertificateFromUrl (string $url, array $certificateProps) : Certificate
    {
        $urlParsed = parse_url($url);
        if (empty($urlParsed['scheme']) || $urlParsed['scheme'] !== 'https') {
            throw new Exception('Provided address is not a HTTPS URL');
        }
        if (empty($urlParsed['host'])) {
            throw new Exception('Provided address is not a valid URL');
        }
        $urlHost = $urlParsed['host'];

        $curlContext = stream_context_create(["ssl" => ["capture_peer_cert" => true]]);
        $curlRequest = stream_socket_client("ssl://$urlHost:443", $errno, $errstr, 5, STREAM_CLIENT_CONNECT, $curlContext);
        $openSslCert = stream_context_get_params($curlRequest);

        if (empty($errstr) === false) {
            throw new Exception($errstr);
        }

        if (empty($openSslCert["options"]["ssl"]['capture_peer_cert'])) {
            throw new Exception('We were unable to capture an SSL certificate when requesting provided URL');
        }

        $content = openssl_x509_parse($openSslCert["options"]["ssl"]['peer_certificate']);
        $dateFrom = Carbon::createFromTimestamp($content['validFrom_time_t'] + (60 * 60));
        $dateTo = Carbon::createFromTimestamp($content['validTo_time_t'] + (60 * 60));

        if ( $content['validFrom_time_t'] > time() || $content['validTo_time_t'] < time()) {
            throw new Exception('Certificate has already expired');
        }

        $name = $urlHost . ' - ' . $dateFrom->format('Y-m-d') . ' → '. $dateTo->format('Y-m-d');

        return new Certificate(array_merge([
            'type' => CertificateTypeEnum::Web,
            'name' => $name,
            'from_date' => $dateFrom,
            'to_date' => $dateTo,
            'content' => json_encode($content),
        ], $certificateProps));
    }

    /**
     * @param string $localFileName
     * @param array $certificateProps
     * @return Certificate|null
     * @throws Exception
     */
    public function getFileCertificate (string $localFileName, array $certificateProps) : ?Certificate
    {
        if ($certificateProps['cert_file']->getClientMimeType() === 'application/x-pkcs12') {
            return $this->getFileCertificateFromP12($localFileName, $certificateProps);
        }

        throw new Exception('Cannot find the functionality to parse this certificate type');
    }

    public function getFileCertificateFromP12 (string $localFileName, array $certificateProps) : Certificate
    {
        $passwordPart = empty($certificateProps['password']) ? '' : '-password pass:' . $certificateProps['password'];

        $exec = exec(
            "openssl pkcs12 -in \"$localFileName\" -nodes $passwordPart | openssl x509 -noout -text",
            $execOutput,
            $execErrCode
        );
        if ($execErrCode === 1 && empty($exec) && empty($execOutput)) {
            throw new Exception('We were unable to extract information from the certificate file. You may need to provide a password');
        }

        $formatted = implode("\n", $execOutput);
        if ($execErrCode === 1) {
            throw new Exception('We were unable to extract information from the certificate file. Following information from OpenSSL: ' . $formatted);
        }

        $execOutput = collect($execOutput);
        $notBefore = null;
        $notAfter = null;
        $subject = null;

        $execOutput->each(function ($line) use (&$notBefore, &$notAfter, &$subject) {
            if ($notAfter !== null && $notBefore !== null && $subject !== null) { return false; }

            preg_match_all('/^[\s]*Not Before[\s]*:[\s]*(\S*)\s*(\S*)\s*(\S*)\s*(\S*)\s*(\S*)$/m', $line, $beforeMatch, PREG_SET_ORDER, 0);
            if (empty($beforeMatch) === false) {
                $notBefore = $beforeMatch[0];
            } else {
                preg_match_all('/^[\s]*Not After[\s]*:[\s]*(\S*)\s*(\S*)\s*(\S*)\s*(\S*)\s*(\S*)$/m', $line, $afterMatch, PREG_SET_ORDER, 0);
                if (empty($afterMatch) === false) {
                    $notAfter = $afterMatch[0];
                } else {
                    preg_match_all('/^[\s]*Subject[\s]*:[\s]*(.*)$/m', $line, $subjectMatch, PREG_SET_ORDER, 0);
                    if (empty($subjectMatch) === false) {
                        $subject = $subjectMatch[0][1];
                    }
                }
            }
        });

        if ($notBefore === null || $notAfter === null) {
            // @todo: improve message
            throw new Exception('We were unable to extract begin and expiry dates from certificate');
        }

        $notBeforeDate = Carbon::createFromFormat('d M Y H:i:s T', $notBefore[2] . ' ' . $notBefore[1] . ' ' . $notBefore[4] . ' '. $notBefore[3] . ' '. $notBefore[5]);
        $notAfterDate = Carbon::createFromFormat('d M Y H:i:s T', $notAfter[2] . ' ' . $notAfter[1] . ' ' . $notAfter[4] . ' '. $notAfter[3] . ' '. $notAfter[5]);

        if (empty($notBeforeDate) || empty($notAfterDate)) {
            // @todo: improve message
            throw new Exception('We were unable to parse the begin and expiry dates from certificate');
        }

        return new Certificate(array_merge([
            'type' => CertificateTypeEnum::File,
            'name' => $subject,
            'from_date' => $notBeforeDate,
            'to_date' => $notAfterDate,
            'file_name' => $certificateProps['cert_file']->getClientOriginalName(),
            'content' => $execOutput->join("\n"),
        ], $certificateProps));
    }

    /**
     * @param UploadedFile $file
     * @return bool
     */
    public function isValidCertificateFileFormat (UploadedFile $file) : bool
    {
        return in_array($file->getClientMimeType(), array_values($this->allowedFormats));
    }

    /**
     * @return string
     */
    public function getInvalidCertificateFileFormatMessage () : string
    {
        return 'Provided file is not of the accepted certificate file formats: ' .
            collect($this->allowedFormats)->keys()->join(', ', ' or ');
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    public function storeFile (UploadedFile $file) : string
    {
        $root = storage_path().'/cert_uploads';
        $disk = Storage::build([ 'driver' => 'local', 'root' => $root ]);
        $filename = $disk->putFile($file->getClientOriginalName(), $file);
        return $root . '/' . $filename;
    }
}
