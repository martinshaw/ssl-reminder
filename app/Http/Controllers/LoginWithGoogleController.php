<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Exception;

class LoginWithGoogleController extends Controller
{
    protected $allowedEmailDomains = [
        '@venditan.com'
    ];

    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleGoogleCallback()
    {
        try {
            $googleUser = Socialite::driver('google')->user();
            $localUser = User::where('google_id', $googleUser->id)->first();

            if (!Str::endsWith($googleUser->email, $this->allowedEmailDomains)) {
                return redirect()
                    ->route('login')
                    ->with(
                        'status',
                        __('Edit functionality is only allowed for internal Venditan email addresses')
                    );
            }

            if ($localUser) {
                Auth::login($localUser);
                return redirect()->intended(route('dashboard'));
            } else {
                $newUser = User::create([
                    'name' => $googleUser->name,
                    'email' => $googleUser->email,
                    'google_id'=> $googleUser->id,
                    'password' => 'password'
                ]);

                Auth::login($newUser);
                return redirect()->intended(route('dashboard'));
            }

        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }
}
