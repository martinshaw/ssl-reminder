<?php
namespace App\Http\Controllers;

use App\Http\Requests\CertificateStoreFileRequest;
use App\Http\Requests\CertificateStoreWebRequest;
use App\Models\Certificate;
use App\Providers\CertificateServiceProvider;
use App\Services\CertificateService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class CertificateController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index ()
    {
        return view('certificates.index', [ 'certificates' => Certificate::orderBy('to_date', 'asc')->get() ]);
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create ()
    {
        return view('certificates.create');
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function createWeb ()
    {
        return view('certificates.createWeb');
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function createFile ()
    {
        return view('certificates.createFile');
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function createManual ()
    {
        return view('certificates.createManual');
    }

    /**
     * @param CertificateStoreWebRequest $request
     * @param CertificateService $service
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeWeb (CertificateStoreWebRequest $request, CertificateService $service)
    {
        $validated = $request->validated();

        try {
            $service->getWebCertificateFromUrl($validated['website_url'], $validated)->save();
            return redirect()->route('certificate.index');
        } catch (\Exception $exception) {
            return back()->withInput($validated)->withErrors([$exception->getMessage()]);
        }
    }

    /**
     * @param CertificateStoreFileRequest $request
     * @param CertificateService $service
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeFile (CertificateStoreFileRequest $request, CertificateService $service)
    {
        $validated = $request->validated();
        $file = $validated['cert_file'];

        if ($service->isValidCertificateFileFormat($file) === false) {
            return back()->withInput($validated)->withErrors([$service->getInvalidCertificateFileFormatMessage()]);
        }

        try {
            $localFileName = $service->storeFile($file);
            $certificate = $service->getFileCertificate($localFileName, $validated)->save();
            return redirect()->route('certificate.index');
        } catch (\Exception $exception) {
            return back()->withInput($validated)->withErrors([$exception->getMessage()]);
        }
    }

    /**
     * @param Certificate $certificate
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show (Certificate $certificate)
    {
        return view('certificates.show', compact('certificate'));
    }

    /**
     * @param Certificate $certificate
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete (Certificate $certificate)
    {
        $certificate->delete();
        return redirect()->route('certificate.index');
    }
}
