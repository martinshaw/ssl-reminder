<?php

namespace App\Enums;

class CertificateTypeEnum
{
    const Web = 'web';
    const File = 'file';
    const Manual = 'manual';

    public static function getAll () {
        return [
            static::Web,
            static::File,
            static::Manual,
        ];
    }
}
