<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <x-application-logo />
        </x-slot>

        <x-jet-validation-errors class="mb-4" />

        @if (session('status'))
            <div class="mb-4 font-medium text-sm text-center text-red-600">
                {{ session('status') }}
            </div>
        @endif

        <div class="flex items-center justify-center py-4">

            <x-jet-button onclick="window.location='{{ url('authorized/google') }}';">
                {{ __('Authenticate with Google') }}
            </x-jet-button>
        </div>
    </x-jet-authentication-card>
</x-guest-layout>
