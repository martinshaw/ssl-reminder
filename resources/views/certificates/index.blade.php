<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Certificates') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg p-6">
                @auth
                    <div class="w-full pb-4 flex justify-between">
                        <x-button class="float-left ">aaa</x-button>
                        <x-button onclick="window.location='{{ route('certificate.create') }}'" class="float-right ">Add Certificate</x-button>
                    </div>
                @endauth

                <table class="w-full table-auto">
                    <thead>
                        <tr>
                            <th class="text-center">Type</th>
                            <th class="text-center">Name</th>
                            <th class="text-center">Start Date</th>
                            <th class="text-center">Expiry Date</th>
                            <th class="text-center">Reminds</th>
                            @auth
                                <th class="text-center"> </th>
                            @endauth
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($certificates ?? [] as $certificate)
                            <tr>
                                <td class="text-center">{{ $certificate->type }}</td>
                                <td class="text-center">{{ $certificate->name }}</td>
                                <td class="text-center">{{ $certificate->from_date }}</td>
                                <td class="text-center">{{ $certificate->to_date }}</td>
                                <td class="text-center">{{ empty($certificate->remind_days) ? 'On Expiry' : $certificate->remind_days . ' days before' }}</td>

                                @auth
                                    <td class="text-center"><x-button onclick="window.location='{{ route('certificate.show', $certificate->id) }}';">View</x-button></td>
                                @endauth
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</x-app-layout>
