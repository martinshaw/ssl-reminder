<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Certificates - ') . $certificate->name }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg p-6">
                <div class="w-full pb-4 flex justify-between">
                    <x-button onclick="window.location='{{ route('certificate.index') }}'" class="float-left ">&larr; Back to All Certificates</x-button>
                    <form action="{{ route('certificate.delete', $certificate->id) }}" method="POST">
                        @method('DELETE')
                        @csrf
                        <x-button>Delete Certificate ✕</x-button>
                    </form>
                    <form action="{{ route('certificate.refresh', $certificate->id) }}" method="POST">
                        @method('PATCH')
                        @csrf
                        <x-button>Refresh Certificate ⟳</x-button>
                    </form>
                    <x-button class="float-right ">Save Changes &rarr;</x-button>
                </div>

                <div class="pb-4">
                    <x-label for="name" value="{{ __('Name: ') }}" />
                    <x-input id="name" class="block mt-1 w-full" type="text" name="name" :value="$certificate->name"/>
                </div>

                <div class="pb-4">
                    <x-label for="remind_days" value="{{ __('How many days before expiry would you like to be reminded?') }}" />
                    <select id="remind_days" class="block mt-1 w-full" name="remind_days" :value="old('remind_days')">
                        <option value="0">On Expiry</option>
                        <option value="2">Remind 2 day before</option>
                        <option value="7">Remind 1 week before</option>
                        <option value="14">Remind 2 weeks before</option>
                        <option value="21">Remind 3 weeks before</option>
                        <option value="30">Remind 30 days before</option>
                        <option value="60">Remind 60 days before</option>
                        <option value="180">Remind 180 days before</option>
                    </select>
                </div>

                <div class="pb-4">
                    <x-label for="notes" value="{{ __('Notes: ') }}" />
                    <textarea id="notes" class="block mt-1 w-full" type="text" rows="5" name="notes">{{ (($certificate->notes)) }}</textarea>
                </div>

                <hr class="py-3" />

                <div class="pb-4">
                    <x-label for="id" value="{{ __('ID: ') }}" />
                    <x-input id="id" class="block mt-1 w-full" type="number" name="id" :value="$certificate->id" disabled="disabled"/>
                </div>

                <div class="pb-4">
                    <x-label for="from_date" value="{{ __('Start Date: ') }}" />
                    <x-input id="from_date" class="block mt-1 w-full" type="text" name="from_date" :value="$certificate->from_date" disabled="disabled"/>
                </div>

                <div class="pb-4">
                    <x-label for="to_date" value="{{ __('Expiry Date: ') }}" />
                    <x-input id="to_date" class="block mt-1 w-full" type="text" name="to_date" :value="$certificate->to_date" disabled="disabled"/>
                </div>

                <div class="pb-4">
                    <x-label for="file_name" value="{{ __('File Name: ') }}" />
                    <x-input id="file_name" class="block mt-1 w-full" type="text" name="file_name" :value="$certificate->file_name" disabled="disabled"/>
                </div>

                <div class="pb-4">
                    <x-label for="content" value="{{ __('Parsed Content: ') }}" />
                    <textarea id="content" class="block mt-1 w-full" type="text" rows="20" name="content" disabled="disabled">{{ trim(stripslashes($certificate->content)) }}</textarea>
                </div>

            </div>
        </div>
    </div>
</x-app-layout>
