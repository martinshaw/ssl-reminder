<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Certificates - Create From Website') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg p-6">
                <div class="w-full pb-6">

                    <form action="{{ route('certificate.store.web') }}" method="POST">
                        @csrf

                        <x-jet-validation-errors class="mb-4" />

                        <div class="pb-4">
                            <x-label for="website_url" value="{{ __('Secure website URL (e.g. https://google.com)') }}" />
                            <x-input id="website_url" class="block mt-1 w-full" type="url" name="website_url" :value="old('website_url')" required autofocus />
                        </div>

                        <div class="pb-4">
                            <x-label for="remind_days" value="{{ __('How many days before expiry would you like to be reminded?') }}" />
                            <select id="remind_days" class="block mt-1 w-full" name="remind_days" :value="old('remind_days')">
                                <option value="0">On Expiry</option>
                                <option value="2">Remind 2 day before</option>
                                <option value="7">Remind 1 week before</option>
                                <option value="14">Remind 2 weeks before</option>
                                <option value="21">Remind 3 weeks before</option>
                                <option value="30">Remind 30 days before</option>
                                <option value="60">Remind 60 days before</option>
                                <option value="180">Remind 180 days before</option>
                            </select>
                        </div>

                        <div class="pb-4">
                            <x-label for="notes" value="{{ __('Notes: ') }}" />
                            <p class="text-sm text-gray-500">Provide information needed to renew the certificate and/or who will need to be notified about renewals</p>
                            <textarea id="notes" class="block mt-1 w-full" type="text" rows="5" name="notes">{{ old('notes') }}</textarea>
                        </div>

                        <div class="w-full">
                            <x-button type="submit">Add Certificate From Website &rarr;</x-button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
