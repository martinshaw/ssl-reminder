<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Certificates - Create From Manual Entry') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg p-6">
                <div class="w-full pb-6">
                    <x-button onclick="window.location='{{ route('certificate.create.web') }}'" class="float-left ">By Website</x-button>
                    <x-button onclick="window.location='{{ route('certificate.create.file') }}'" class="float-left ">By File</x-button>
                    <x-button onclick="window.location='{{ route('certificate.create.manual') }}'" class="float-left ">Manual Entry</x-button>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
