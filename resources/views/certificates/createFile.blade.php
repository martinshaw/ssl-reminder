<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Certificates - Create From File') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg p-6">
                <div class="w-full pb-6">

                    <form action="{{ route('certificate.store.file') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        <x-jet-validation-errors class="mb-4" />

                        <div class="pb-4">
                            <x-label for="cert_file" value="{{ __('Certificate File:') }}" />
                            <x-input id="cert_file" class="block mt-1 w-full" type="file" name="cert_file" :value="old('cert_file')" required autofocus />
                        </div>

                        <div class="pb-4">
                            <x-label for="password" value="{{ __('Certificate Password: (optional)') }}" />
                            <x-input id="password" class="block mt-1 w-full" type="text" name="password" :value="old('password')" />
                        </div>

                        <div class="pb-4">
                            <x-label for="remind_days" value="{{ __('How many days before expiry would you like to be reminded?') }}" />
                            <select id="remind_days" class="block mt-1 w-full" name="remind_days" :value="old('remind_days')">
                                <option value="0">On Expiry</option>
                                <option value="2">Remind 2 day before</option>
                                <option value="7">Remind 1 week before</option>
                                <option value="14">Remind 2 weeks before</option>
                                <option value="21">Remind 3 weeks before</option>
                                <option value="30">Remind 30 days before</option>
                                <option value="60">Remind 60 days before</option>
                                <option value="180">Remind 180 days before</option>
                            </select>
                        </div>

                        <div class="pb-4">
                            <x-label for="notes" value="{{ __('Notes: (optional)') }}" />
                            <p class="text-sm text-gray-500">Provide information needed to renew the certificate and/or who will need to be notified about renewals</p>
                            <textarea id="notes" class="block mt-1 w-full" type="text" rows="5" name="notes">{{ old('notes') }}</textarea>
                        </div>

                        <div class="w-full">
                            <x-button type="submit">Add Certificate From File &rarr;</x-button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
