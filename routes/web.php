<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'App\Http\Controllers\DashboardController@index')->name('dashboard');

require __DIR__ . '/certificates/web.php';

Route::get('authorized/google', 'App\Http\Controllers\LoginWithGoogleController@redirectToGoogle')->name('auth.google.redirect');
Route::get('authorized/google/callback', 'App\Http\Controllers\LoginWithGoogleController@handleGoogleCallback')->name('auth.google.callback');

require __DIR__.'/auth.php';
