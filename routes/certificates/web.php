<?php
use Illuminate\Support\Facades\Route;

Route::get('/certficates', 'App\Http\Controllers\CertificateController@index')
    ->name('certificate.index');

Route::get('/certficates/create', 'App\Http\Controllers\CertificateController@create')
    ->middleware('auth')
    ->name('certificate.create');

Route::get('/certficates/create/web', 'App\Http\Controllers\CertificateController@createWeb')
    ->middleware('auth')
    ->name('certificate.create.web');

Route::get('/certficates/create/file', 'App\Http\Controllers\CertificateController@createFile')
    ->middleware('auth')
    ->name('certificate.create.file');

Route::get('/certficates/create/manual', 'App\Http\Controllers\CertificateController@createManual')
    ->middleware('auth')
    ->name('certificate.create.manual');

Route::post('/certficates/web', 'App\Http\Controllers\CertificateController@storeWeb')
    ->middleware('auth')
    ->name('certificate.store.web');

Route::post('/certficates/file', 'App\Http\Controllers\CertificateController@storeFile')
    ->middleware('auth')
    ->name('certificate.store.file');

Route::get('/certficates/{certificate}', 'App\Http\Controllers\CertificateController@show')
    ->middleware('auth')
    ->name('certificate.show');

Route::patch('/certficates/{certificate}/refresh', 'App\Http\Controllers\CertificateController@refresh')
    ->middleware('auth')
    ->name('certificate.refresh');

Route::delete('/certficates/{certificate}', 'App\Http\Controllers\CertificateController@delete')
    ->middleware('auth')
    ->name('certificate.delete');
